package com.android.mycamera.cameraapplication;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.mycamera.cameraapplication.adapter.ListAdapter;
import com.android.mycamera.cameraapplication.dataobjects.MyDocument;
import com.android.mycamera.cameraapplication.util.MyCameraApplicationUtil;
import com.scanlibrary.IScanner;
import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends Activity {

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private static final int ANDROID_GALLERY_ACTIVITY = 101;
    public static final int LAUNCH_THUMBNAIL = 102;
    public static final String LOG_TAG = "MyCameraAppLog";

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 7;

    private ListView documentListView;
    private ListAdapter listAdapter;
    private IScanner scanner;
    private ArrayList<MyDocument> documents = new ArrayList<MyDocument>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkAndroidVersion();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
        documentListView = (ListView) findViewById(R.id.documentListView);
        listAdapter = new ListAdapter(this);

        getAllStoredDocuments();

        findViewById(R.id.cameraButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                launchCamera();
            }
        });

        findViewById(R.id.galleryButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchGallery();
            }
        });


        documentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int arg2,
                                    long arg3) {

                Log.i(LOG_TAG, "clicked tag -- > " + view.getTag());
                String path = getExternalCacheDir().getPath();
                String documentName = (String) view.getTag();

                String fullDocPath = path + "/" + documentName;
                Log.i(LOG_TAG, "Full Document Path:  " + fullDocPath);


                launchThumbnailView(2, documentName);
            }
        });

        registerForContextMenu(documentListView);
    }

    private void checkAndroidVersion() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();

        } else {
            // code for lollipop and pre-lollipop devices
        }

    }

    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.CAMERA);
        int wtite = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (wtite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("in fragment on request", "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("in fragment on request", "CAMERA & WRITE_EXTERNAL_STORAGE READ_EXTERNAL_STORAGE permission granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d("in fragment on request", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Camera and Storage Permission required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(getApplicationContext(), "Go to settings and enable permissions", Toast.LENGTH_LONG)
                                    .show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getApplicationContext())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAllStoredDocuments();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.delete_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        MyDocument document = (MyDocument) listAdapter.getItem((int) info.id);
        String documentName = document.getDocumentName();


        switch (item.getItemId()) {

            case R.id.deleteDoc:
                deleteDocument(documentName);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void deleteDocument(String documentName) {
        //   File fileDir =  getApplicationContext().getDir("files", Context.MODE_PRIVATE);
        File documentDir = new File(getExternalCacheDir(), documentName);
        Log.i(LOG_TAG, "documen dir path " + documentDir.getAbsolutePath());
        Log.i(LOG_TAG, "document Dir before " + documentDir.exists());
        deleteDirectory(documentDir);
        documentDir.delete();

        MyDocument doc = new MyDocument();
        doc.setDocumentName(documentName);
        documents.remove(doc);
        getAllStoredDocuments();
    }

    public void deleteDirectory(File directory) {
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                deleteDirectory(file);
            }
            if (file.isFile()) {
                file.delete();
            }
        }
        directory.delete();

    }

    private void launchThumbnailView(int keyValue, String documentPath) {

        Intent intent = new Intent(getApplicationContext(), ImageGridViewActivity.class);
        intent.putExtra("key", keyValue);
        intent.putExtra("documentPath", documentPath);
        startActivityForResult(intent, LAUNCH_THUMBNAIL);
    }

    private void getAllStoredDocuments() {

        ArrayList<MyDocument> documents = new ArrayList<MyDocument>();
        File sourceDirectory = getExternalCacheDir();
        if (!sourceDirectory.exists()) {
            Log.e(LOG_TAG, "Unable to read directory ");

        } else {
            File[] files = sourceDirectory.listFiles();
            int id = 1;

            for (File file : files) {

                Log.i(LOG_TAG, "file get path " + file.getAbsolutePath());
                if (file.isDirectory()) {
                    MyDocument doc = new MyDocument();
                    doc.setDocumentName(file.getName());
                    doc.setId(id);

                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
                    String modifiedDate = sdf.format(file.lastModified());
                    Log.d(LOG_TAG, "After Format : " + modifiedDate);
                    try {
                        doc.setDateModified(sdf.parse(modifiedDate));
                    } catch (ParseException e) {

                    }
                    doc.setModifiedDate(modifiedDate);
                    /*find files count in original folder and set to document object*/
                    if (file.listFiles() != null && file.listFiles().length > 0) {
                        int filesCount = file.listFiles().length;
                        Log.i(LOG_TAG, "filesCount " + filesCount);
                        doc.setNumberOfPages(filesCount);
                        String firstFilePath = MyCameraApplicationUtil.getFirstFilePath(file.getAbsolutePath());
                        doc.setThumbnailPath(firstFilePath);
                    } else {
                        doc.setNumberOfPages(0);
                    }

                    if (!documents.contains(doc)) {
                        documents.add(doc);
                        id++;
                    }

                }
            }
        }
        Collections.sort(documents);

        for (MyDocument doc : documents) {
            Log.i(LOG_TAG, "doc date in order " + doc.getDocumentName());
        }

        listAdapter.setDocumentList(documents);
        documentListView.setAdapter(listAdapter);
    }

    private void launchCamera() {

        // create Intent to take a picture and return control to the calling application
        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, ScanConstants.OPEN_CAMERA);

        File cacheDir = getApplicationContext().getCacheDir();
        Log.e(LOG_TAG, cacheDir.getAbsolutePath() + "/" + MyCameraApplicationUtil.getDirectoryName());
        // start the image capture Intent
        startActivityForResult(intent, ScanConstants.START_CAMERA_REQUEST_CODE);

    }

    public void launchGallery() {
        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, ScanConstants.OPEN_MEDIA);
        startActivityForResult(intent, ScanConstants.PICKFILE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Uri uri = null;
        if ((requestCode == ScanConstants.PICKFILE_REQUEST_CODE || requestCode == ScanConstants.START_CAMERA_REQUEST_CODE) &&
                resultCode == Activity.RESULT_OK) {
            uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
            try {
                final Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                Uri fileUri = MyCameraApplicationUtil.getOutputMediaFileUri(MyCameraApplicationUtil.getDirectoryName(), MyCameraApplicationUtil.MEDIA_TYPE_IMAGE, getApplicationContext()); // create a file to save the image
                File file = new File(fileUri.getPath());
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        /*
        switch (requestCode) {
            case ScanConstants.START_CAMERA_REQUEST_CODE:


                getAllStoredDocuments();
                break;
            case ANDROID_GALLERY_ACTIVITY:

                File tempFile = null;

                Log.d("Image", String.valueOf(uri));
                Uri imgUri = data.getData();

                Log.d(LOG_TAG, " prints img URI " + imgUri);
                String uriString = imgUri.toString();
                if (uriString.endsWith(".png")) {
                    Toast.makeText(getApplicationContext(), "Application allows only jpeg image", Toast.LENGTH_SHORT).show();
                }
                // Handles Kitkat gallery issue
                else if (uriString.contains("com.android.providers.media.documents")) {

                    tempFile = MyCameraApplicationUtil.getFileFromURIKitkat(imgUri, getContentResolver());
                } else {
                    tempFile = MyCameraApplicationUtil.getFileFromURI(imgUri, getContentResolver());
                }

                FileInputStream fis = null;
                File cacheDir = getApplicationContext().getCacheDir();
                File imageFile = MyCameraApplicationUtil.getOutputMediaFile(MyCameraApplicationUtil.getDirectoryName(), MyCameraApplicationUtil.MEDIA_TYPE_IMAGE, getApplicationContext());
                if (tempFile != null && tempFile.exists()) {

                    try {
                        imageFile.createNewFile();
                        imageFile.setReadable(true, true);
                        imageFile.setWritable(true, true);
                    } catch (IOException e) {

                        System.out
                                .println("exception in file copy to project folder ");
                        e.printStackTrace();
                    }

                    FileOutputStream fos = null;
                    try {
                        fis = new FileInputStream(tempFile);
                        fos = new FileOutputStream(imageFile);
                        Log.i(MyCameraApplicationUtil.LOG_TAG, "fis " + fis);
                    } catch (FileNotFoundException e) {

                        e.printStackTrace();
                    }
                    byte[] buffer = new byte[1024];
                    int read;
                    try {
                        while ((read = fis.read(buffer)) != -1) {
                            fos.write(buffer, 0, read);
                        }
                        fos.close();
                        fis.close();
                    } catch (IOException e) {

                        e.printStackTrace();
                    }
                }
                //Refresh documents list
                getAllStoredDocuments();
                break;

        }
        */
        super.onActivityResult(requestCode, resultCode, data);

    }

}
