package com.android.mycamera.cameraapplication.util;

import java.io.FileOutputStream;

public interface FileWritingCallback {

    public void write(FileOutputStream out);
}
