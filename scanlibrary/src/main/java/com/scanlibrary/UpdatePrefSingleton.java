package com.scanlibrary;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class UpdatePrefSingleton {
    private static UpdatePrefSingleton mInstance;
    private Context mContext;
    //
    public SharedPreferences mMyPreferences;

    private UpdatePrefSingleton(){ }

    public static UpdatePrefSingleton getInstance(){
        if (mInstance == null) mInstance = new UpdatePrefSingleton();
        return mInstance;
    }

    public void Initialize(Context ctxt){
        mContext = ctxt;
        mMyPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
    }
    public boolean readPreferenceBolean(String key){
       return mMyPreferences.getBoolean(key, false);
    }
    public void writePreferenceBol(String key, boolean value){
        SharedPreferences.Editor e = mMyPreferences.edit();
        e.putBoolean(key, value);
        e.commit();
    }
    public String readPreferenceString(String key){
        return mMyPreferences.getString(key, "Tracking");
    }
    public void writePreferenceString(String key, String value){
        SharedPreferences.Editor e = mMyPreferences.edit();
        e.putString(key, value);
        e.commit();
    }
}
